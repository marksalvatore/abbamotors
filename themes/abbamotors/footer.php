<?php $upload_dir = wp_upload_dir(); ?>

</div><!-- site-content -->


<div class="footer-hr"></div>

<footer class="site-footer" role="contentinfo">

  <div class="footer-top">
    <div class="address">
        Call us at <strong><a href="tel:+4135846128">(413) 584 6128</a></strong>, email <a href="mailto:info@abbamotors.com">info@abbamotors.com</a> or stop by 30 N. Maple St., Florence, MA. We're open Monday&mdash;Thursday 8:00am&mdash;5:30pm.
    </div>

    <div class="asa-certified">
      <img src="<?php echo $upload_dir['baseurl'];?>/2015/11/Box_CarBrands_ASAfix-1.png"</a>
    </div>
  </div>

  <div class="footer-bottom">
    <div class="social">
        <a href="https://www.facebook.com/Abba-Motors-226652824117861/"><img src="<?php echo $upload_dir['baseurl'];?>/2015/10/Logo_Facebook.png"></a> <span>&copy; <?=date("Y");?> Abba Motors. All Rights Reserved.</span>
    </div>

    <div class="asa">
      <img src="<?php echo $upload_dir['baseurl'];?>/2015/10/Logo_Asa.png"</a>
    </div>
  </div>

</footer>

<?php wp_footer(); ?>

</div><!-- site-wrapper -->
</body>
</html>
