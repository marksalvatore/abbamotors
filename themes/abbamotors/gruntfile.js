module.exports = function(grunt) {
  grunt.initConfig({
        
    concat : {
      jshead : { // task name
        src : ['scripts/header/*.js'],
        dest : 'js/headerscripts.js'
      },
      jsfoot : { // task name
        src : ['scripts/footer/*.js'],
        dest : 'js/footerscripts.js'
      },
    },

    uglify: {
      jshead: {
        files: {
          'js/headerscripts.js' : 'js/headerscripts.js'
        }
      },
      jsfoot: {
        files: {
          'js/footerscripts.js' : 'js/footerscripts.js'
        }
      },
    },
    
		compass: {
			dist: {
				options: {
					sassDir: 'sass',
					cssDir: 'css',
				}
			},
		},
		
    imageoptim: {
      task: {
        src: ['../../uploads', 'images']
      }
    },
    
    postcss: {
      options: {
        map: true, // inline sourcemaps
  
        processors: [
          //require('pixrem')(), // add fallbacks for rem units
          require('autoprefixer')({browsers: 'last 2 versions'}), // add vendor prefixes
          //require('cssnano')() // minify the result
        ]
      },
      dist: {
        src: 'css/style.css'
      }
    },
    
    cssmin: {
      build: {
        src: 'css/style.css',
        dest: 'style.css'
      }
    },

    watch: {
      jshead: {
        files: [
          'scripts/header/*.js',
        ],
        tasks: ['concat', 'uglify']
      },
      jsfoot: {
        files: [
          'scripts/footer/*.js',
        ],
        tasks: ['concat', 'uglify']
      },
      sass: {
        files: [
          'sass/**/*.scss'
        ],
        tasks: ['compass', 'postcss', 'cssmin']
      },
      images: {
        files: [
          '../../uploads/**/*.png'
        ],
        tasks: ['imageoptim']
      }
    }

  }); //initConfig
  
  
  // run tasks
  grunt.loadNpmTasks('grunt-contrib-concat'); 
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-imageoptim');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['concat', 'uglify', 'compass', 'postcss', 'cssmin']);
  

}; //wrapper function