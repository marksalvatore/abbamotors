<?php
/**
 * Template Name: Service
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package abbamotors
 */

get_header(); 

$custom_fields = get_post_custom(); // get all custom fields

while ( have_posts() ) : the_post(); 

  get_template_part( 'template-parts/service', 'overview' ); 
  get_template_part( 'template-parts/service', 'diagnostics' ); 
  get_template_part( 'template-parts/service', 'maintenance' ); 
  get_template_part( 'template-parts/service', 'hybrids' ); 
  get_template_part( 'template-parts/service', 'repairs' ); 
  get_template_part( 'template-parts/service', 'repairs-detail' ); 

endwhile; // End of the loop. 

get_footer(); 


