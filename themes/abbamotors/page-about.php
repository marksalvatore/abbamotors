<?php
/**
 * Template Name: About
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package abbamotors
 */

get_header();

$custom_fields = get_post_custom(); // get all custom fields

while ( have_posts() ) : the_post();

  get_template_part( 'template-parts/about', 'overview' );
  get_template_part( 'template-parts/about', 'history' );
  get_template_part( 'template-parts/about', 'staff' );
  get_template_part( 'template-parts/about', 'associations' );

endwhile; // End of the loop.

get_footer();
