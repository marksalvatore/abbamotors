<?php
/**
 * Template Name: Roadwise
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package abbamotors
 */

get_header();

$custom_fields = get_post_custom(); // get all custom fields

while ( have_posts() ) : the_post();

  get_template_part( 'template-parts/roadwise', 'overview' );
  get_template_part( 'template-parts/roadwise', 'tips' );
  get_template_part( 'template-parts/roadwise', 'tipslist' );

endwhile; // End of the loop.

get_footer();


