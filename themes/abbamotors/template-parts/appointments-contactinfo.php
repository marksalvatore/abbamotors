<?php
/**
 * Template segment for the appointments page
 * @package abbamotors
 */

global $custom_fields;
$contactinfo_graphic = $custom_fields['contactinfo_graphic'];
$contactinfo_text = $custom_fields['contactinfo_text'];
$contactinfo_image = $custom_fields['contactinfo_image'];
?>


<section class="contactinfo"> 
  <div class="graphic"><?php 
    if ( !empty($contactinfo_graphic[0]) ) {
      echo wp_get_attachment_image($contactinfo_graphic[0], 'full');
    } ;?>
  </div>
  <div class="text"><?php echo $contactinfo_text[0];?></div>
  <div class="image"><?php 
    if ( !empty($contactinfo_image[0]) ) {
      echo wp_get_attachment_image($contactinfo_image[0], 'full');
    } ?>
  </div>  
</section>