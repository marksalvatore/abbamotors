<?php
/**
 * Template segment for a roadwise page
 * @package abbamotors
 */

	global $custom_fields;
	$tips_title = $custom_fields['tips_title'];
	$tips_text = $custom_fields['tips_text'];
	$tips_image = $custom_fields['tips_image'];

	$upload_dir = wp_upload_dir();
	$season = abbamotors_determineSeason();
?>


<section class="roadwise">

  <div class="title"><h1><?php echo $tips_title[0];?></h1></div>
  <div class="text"><?php echo $tips_text[0];?></div>
  <div class="image">
		<?php
		//echo wp_get_attachment_image($tips_image[0], 'full');
		echo "<img src='".$upload_dir['baseurl']."/2015/10/$season.png' width='203' height='203' alt='Roadwise Tips: $season'>";
		?>

  </div>

</section>
