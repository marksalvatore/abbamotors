<?php
/**
 * Template segment for the home page
 * @package abbamotors
 */

global $custom_fields;
$overview_title = $custom_fields['overview_title'];
$overview_text = $custom_fields['overview_text'];
$overview_image = $custom_fields['overview_image'];
$overview_graphic = $custom_fields['overview_graphic'];
?>


<section class="overview">
  <div class="image"><?php
    if ( !empty($overview_image[0]) ) {
      echo wp_get_attachment_image($overview_image[0], 'full');
    } ?>
  </div>
  <div class="title"><h1><?php echo $overview_title[0];?></h1></div>
  <div class="subflex">
    <div class="text">
      <?php echo $overview_text[0];
      echo "<div class='graphic-textonly'>";
        echo "<a href='get_site_url()/service'>Learn more</a>";
      echo "</div>";?>
    </div>
    <?php if ( !empty($overview_graphic[0]) ) {
      echo "<div class='graphic'>";
        echo "<a href='get_site_url()/service'>";
        echo wp_get_attachment_image($overview_graphic[0], 'full');
      echo "</a></div><br>";
    } ?>
  </div>
</section>

<span class="footer-hr"></span>

<section class="info">


  <div class="hours">
    Monday&nbsp;&mdash;&nbsp;Thursday, 8:00am&nbsp;&mdash;&nbsp;5:30pm
  </div>

  <div class="image">
	  <?php $upload_dir = wp_upload_dir(); ?>
	  <img src="<?php echo $upload_dir['baseurl'];?>/2015/10/Circle_Beetle.png"</a>
  </div>

  <div class="address">
    Call us at <strong><a href="tel:+4135846128">(413) 584 6128</a></strong>, email <a href="mailto:info@abbamotors.com">info@abbamotors.com</a> or stop by 30 N. Maple St., Florence, MA.
  </div>

</section>
