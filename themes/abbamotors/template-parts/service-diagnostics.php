<?php
/**
 * Template segment for the service page
 * @package abbamotors
 */

global $custom_fields;
$diagnostics_title = $custom_fields['diagnostics_title'];
$diagnostics_text = $custom_fields['diagnostics_text'];
$diagnostics_image = $custom_fields['diagnostics_image'];
?>

<section class="diagnostics">
  
  <div class="text oncolorbg">
    <h2><?php echo $diagnostics_title[0];?></h2>
    <?php echo $diagnostics_text[0];?>
  </div>
  <div class="image"><?php 
    if ( !empty($diagnostics_image[0]) ) {
      echo wp_get_attachment_image($diagnostics_image[0], 'full');
    } ?>
  </div><br />
  
</section>