<?php
/**
 * Template segment for the contact page
 * @package abbamotors
 */

global $custom_fields;
$directions_title = $custom_fields['directions_title'];
$directions_text = $custom_fields['directions_text'];
$directions_image = $custom_fields['directions_image'];
?>


<section class="directions">
  
  <div class="text oncolorbg">
    <h2><?php echo $directions_title[0];?></h2>
    <?php //echo $directions_text[0];?>
    
    <div class="iframe">
      <!-- Google embed code -->
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2949.259185154561!2d-72.67460908454437!3d42.336997179188465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e6d7a51ef6f59d%3A0xa10a51533b2104d2!2s30+N+Maple+St%2C+Florence%2C+MA+01062!5e0!3m2!1sen!2sus!4v1444759022873" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    
  </div>
  <div class="image"><?php 
    if ( !empty($directions_image[0]) ) {
      echo wp_get_attachment_image($directions_image[0], 'full');
    } ?>
  </div><br />
  
</section>