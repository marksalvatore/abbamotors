<?php
/**
 * Template segment for the contact page
 * @package abbamotors
 */

global $custom_fields;
$overview_text = $custom_fields['overview_text'];
$overview_image = $custom_fields['overview_image'];
?>


<section class="overview">
  
  <div class="image"><?php 
    if ( !empty($overview_image[0]) ) {
      echo wp_get_attachment_image($overview_image[0], 'full');
    } ;?>
  </div>
  <div class="text"><?php echo $overview_text[0];?></div>
  
</section>