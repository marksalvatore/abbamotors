<?php
/**
 * Template segment for the service page
 * @package abbamotors
 */

global $custom_fields;
$repairsdetail_text = $custom_fields['repairs_detail_text'];
$repairsdetail_image = $custom_fields['repairs_detail_image'];
?>

<section class="repairsdetail">
  
  <div class="text oncolorbg"><?php echo $repairsdetail_text[0];?></div>
  <div class="image"><?php 
    if ( !empty($repairsdetail_image[0]) ) {
      echo wp_get_attachment_image($repairsdetail_image[0], 'full');
    } ;?>
  </div>
  
</section>