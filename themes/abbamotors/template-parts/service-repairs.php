<?php
/**
 * Template segment for the service page
 * @package abbamotors
 */

global $custom_fields;
$repairs_title = $custom_fields['repairs_title'];
$repairs_text = $custom_fields['repairs_text'];
$repairs_image = $custom_fields['repairs_image'];
?>

<div class="hr"></div>

<section class="repairs">
  
  <div class="title"><h2><?php echo $repairs_title[0];?></h2></div>
  <div class="text"><?php echo $repairs_text[0];?></div>
  <div class="image"><?php 
    if ( !empty($repairs_image[0]) ) {
      echo wp_get_attachment_image($repairs_image[0], 'full');
    } ?>
  </div>
  
</section>