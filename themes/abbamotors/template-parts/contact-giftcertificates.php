<?php
/**
 * Template segment for the contact page
 * @package abbamotors
 */

global $custom_fields;
$giftcertificates_title = $custom_fields['giftcertificates_title'];
$giftcertificates_text = $custom_fields['giftcertificates_text'];
$giftcertificates_image = $custom_fields['giftcertificates_image'];
?>


<section class="giftcertificates">
  
  <div class="image"><?php 
    if ( !empty($giftcertificates_image[0]) ) {
      echo wp_get_attachment_image($giftcertificates_image[0], 'full');
    } ;?>
  </div>
  <div class="text">
    <h2><?php echo $giftcertificates_title[0];?></h2>
    <?php echo $giftcertificates_text[0];?>
  </div>

  
</section>