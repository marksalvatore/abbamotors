<?php
/**
 * Template segment for the service page
 * @package abbamotors
 */

global $custom_fields;
$overview_title = $custom_fields['overview_title'];
$overview_text = $custom_fields['overview_text'];
$overview_image = $custom_fields['overview_image'];
?>


<section class="overview"> 
  <div class="image"><?php 
    if ( !empty($overview_image[0]) ) {
      echo wp_get_attachment_image($overview_image[0], 'full');
    } ;?>
  </div>
  <div class="subflex">
    <div class="title"><h1><?php echo $overview_title[0];?></h1></div>
    <div class="text"><?php echo $overview_text[0];?></div>
  </div>  
</section>