<?php
/**
 * Template segment for the service page
 * @package abbamotors
 */

global $custom_fields;
$hybrids_title = $custom_fields['hybrids_title'];
$hybrids_text = $custom_fields['hybrids_text'];
$hybrids_image = $custom_fields['hybrids_image'];
?>


<section class="hybrids">
  <div class="image"><?php 
    if ( !empty($hybrids_image[0]) ) {
      echo wp_get_attachment_image($hybrids_image[0], 'full');
    } ;?>
  </div>

  <div class="text">
    <h2><?php echo $hybrids_title[0];?></h2>
    <?php echo $hybrids_text[0];?>
  </div>
</section>