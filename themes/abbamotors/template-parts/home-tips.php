<?php
/**
 * Template segment for the home page
 * @package abbamotors
 */

	global $custom_fields;
	$tips_title = $custom_fields['tips_title'];
	$tips_text = $custom_fields['tips_text'];
	$tips_image = $custom_fields['tips_image'];
	$tips_graphic = $custom_fields['tips_graphic'];
	$tips_medallion = $custom_fields['tips_medallion'];

	$upload_dir = wp_upload_dir();
	$season = abbamotors_determineSeason();
?>

<section class="tips">
  <div class="image">
 		<?php
    //echo wp_get_attachment_image($tips_image[0], 'full');
    $season = strtolower($season);
    echo "<img src='".$upload_dir['baseurl']."/2015/11/Roadwise_$season.png' width='341' height='215' alt='Roadwise Tips: $season'>";
     ?>
  </div>
  <div class="subflex">
    <div class="title"><h2><?php echo $tips_title[0];?></h2></div>
    <div class="text oncolorbg"><?php echo $tips_text[0];?></div>
    <?php
    if ( !empty($tips_graphic[0]) ) {
      echo "<div class='graphic'>";
      echo "<a href='get_site_url()/roadwise-$season'>";
      echo wp_get_attachment_image($tips_graphic[0], 'full');
      echo "</div></a><br>";
    } ?>
  </div>
  <?php
  if ( !empty($tips_medallion[0]) ) {
    echo "<div class='medallion'>";
    echo "<a href='http://www.angieslist.com/'>".wp_get_attachment_image($tips_medallion[0], 'full')."</a>";
    echo "</div>";
  } ?>

</section>
