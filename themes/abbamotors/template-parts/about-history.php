<?php
/**
 * Template segment for the about page
 * @package abbamotors
 */

global $custom_fields;
$history_title = $custom_fields['history_title'];
$history_text = $custom_fields['history_text'];
$history_image = $custom_fields['history_image'];
?>


<section class="history">
  <div class="image"><?php 
    if ( !empty($history_image[0]) ) {
      echo wp_get_attachment_image($history_image[0], 'full');
    } ;?>
  </div>

  <div class="text oncolorbg">
    <h2><?php echo $history_title[0];?></h2>
    <?php echo $history_text[0];?>
  </div>
</section>