<?php
/**
 * Template segment for the about page
 * @package abbamotors
 */

global $custom_fields;
$associations_title = $custom_fields['associations_title'];
$associations_text = $custom_fields['associations_text'];
$associations_image = $custom_fields['associations_image'];
?>


<section class="associations">
  <div class="title"><h2><?php echo $associations_title[0];?></h2></div>
  <div class="text"><?php echo $associations_text[0];?></div>
  <div class="image"><?php 
    if ( !empty($associations_image[0]) ) {
      echo wp_get_attachment_image($associations_image[0], 'full');
    } ?>
  </div>  
</section>
