<?php
/**
 * Template segment for a roadwise page
 * @package abbamotors
 */

global $custom_fields;
$tipslist_text = $custom_fields['tipslist_text'];
?>

<section class="tipslist">

  <div class="text"><?php
    if ( !empty($tipslist_text[0]) ) {
      echo $tipslist_text[0];
    } ?>
  </div>

</section>
