<?php
/**
 * Template segment for the about page
 * @package abbamotors
 */
 
global $custom_fields;
$staff_title = $custom_fields['staff_title'];
$staff_text = $custom_fields['staff_text'];
$staff_image = $custom_fields['staff_image'];
?>


<section class="staff">
  <div class="subflex">
    <div class="title"><h2><?php echo $staff_title[0];?></h2></div>
    <div class="text oncolorbg"><?php echo $staff_text[0];?></div>
  </div>
  <div class="image"><?php 
    if ( !empty($staff_image[0]) ) {
      echo wp_get_attachment_image($staff_image[0], 'full');
    } ?>
  </div>
</section>

