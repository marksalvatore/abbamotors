<?php
/**
 * Template segment for the service page
 * @package abbamotors
 */

global $custom_fields;
$maintenance_title = $custom_fields['maintenance_title'];
$maintenance_text = $custom_fields['maintenance_text'];
$maintenance_image = $custom_fields['maintenance_image'];
?>


<section class="maintenance">
  
  <div class="title"><h2><?php echo $maintenance_title[0];?></h2></div>
  <div class="text oncolorbg"><?php echo $maintenance_text[0];?></div>
  <div class="image"><?php 
    if ( !empty($maintenance_image[0]) ) {
      echo wp_get_attachment_image($maintenance_image[0], 'full');
    } ;?>
  </div>
  
</section>