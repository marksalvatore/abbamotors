<?php
/**
 * Template segment for a roadwise page
 * @package abbamotors
 */

global $custom_fields;
$overview_image = $custom_fields['overview_image'];
?>


<section class="overview">
  
  <div class="image"><?php 
    if ( !empty($overview_image[0]) ) {
      echo wp_get_attachment_image($overview_image[0], 'full');
    } ;?>
  </div>
  
</section>