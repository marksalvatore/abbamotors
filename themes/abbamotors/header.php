<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href='https://fonts.googleapis.com/css?family=Oswald:700|Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
  <?php /* Conditional sheets
    <link rel='stylesheet' media='screen and (min-width: 701px) and (max-width: 900px)' href='css/medium.css' />
  */ ?>
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div class="site-wrapper">  
  	<header class="site-header" role="banner">
  		<nav class="site-navigation" role="navigation">		
  			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu') ); ?>
  		</nav>
  		
  		<div class="site-branding">
  			<div class="site-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
  			  <?php $upload_dir = wp_upload_dir(); ?>
  			  <img src="<?php echo $upload_dir['baseurl'];?>/2015/10/Logo_Abba.png"</a>
  			</div>
  			<a class="toggleMenu" href="#">☰</a>
  		</div><!-- .site-branding -->
  
  	</header>

    <div class="site-content">
