<?php
/**
 * abbamotors functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package abbamotors
 */

if ( ! function_exists( 'abbamotors_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function abbamotors_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on abbamotors, use a find and replace
	 * to change 'abbamotors' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'abbamotors', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'abbamotors' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'abbamotors_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // abbamotors_setup
add_action( 'after_setup_theme', 'abbamotors_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function abbamotors_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'abbamotors_content_width', 640 );
}
add_action( 'after_setup_theme', 'abbamotors_content_width', 0 );


/**
 * Enqueue scripts and styles.
 */
function abbamotors_styles() {
	wp_enqueue_style( 'abbamotors-style', get_stylesheet_uri() );
	//wp_enqueue_style( 'dashicons' );

}
add_action( 'wp_enqueue_scripts', 'abbamotors_styles' );


function abbamotors_adding_scripts() {
  wp_register_script('headerscripts', get_template_directory_uri() . '/js/headerscripts.js','','1.1', false);
  wp_enqueue_script('headerscripts');

  wp_register_script('footerscripts', get_template_directory_uri() . '/js/footerscripts.js','','1.1', true);
  wp_enqueue_script('footerscripts');
}
add_action( 'wp_enqueue_scripts', 'abbamotors_adding_scripts' );

function abbamotors_determineSeason() {
	// Determine Season
	  $today = getdate(time());
	  //$today = getdate(time() + (200 * 24 * 60 * 60));
	  $month = $today[mon]; // 1 - 12
	 	$quarter = floor($month/3); // 0 - 3

	  if ( $today[mday] > 20) {
	  	// recalculate season
	  	$month++;
	  	$quarter = floor($month/3);
	  }
	  switch ($quarter) {
		  case 1: $season  = 'Spring'; break;
		  case 2: $season  = 'Summer'; break;
		  case 3: $season  = 'Fall'; break;
		  default: $season = 'Winter'; break;
	  }
	  return $season;
}
