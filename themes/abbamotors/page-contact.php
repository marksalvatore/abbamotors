<?php
/**
 * Template Name: Contact
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package abbamotors
 */

get_header(); 

$custom_fields = get_post_custom(); // get all custom fields

while ( have_posts() ) : the_post(); 

  get_template_part( 'template-parts/contact', 'overview' ); 
  get_template_part( 'template-parts/contact', 'directions' ); 
  get_template_part( 'template-parts/contact', 'giftcertificates' ); 

endwhile; // End of the loop. 

get_footer(); 


