<?php
/**
 * Template Name: Appointments
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package abbamotors
 */

get_header(); 

$custom_fields = get_post_custom(); // get all custom fields

while ( have_posts() ) : the_post(); 

  get_template_part( 'template-parts/appointments', 'overview' ); 
  get_template_part( 'template-parts/appointments', 'contactinfo' ); 

endwhile; // End of the loop. 

get_footer(); 


